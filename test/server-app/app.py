from flask import Flask, request
import os
import datetime

app = Flask(__name__)

@app.route('/')
def index():
    return "Serving container: %s\r\nClient IP: %s\r\nTimestamp: %s\r\nClient request headers:\r\n%s" % (os.environ.get('HOSTNAME'),str(request.remote_addr),str(datetime.datetime.now()),str(request.headers))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
