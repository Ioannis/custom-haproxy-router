# Custom HAProxy router

Reference: https://docs.openshift.org/latest/install_config/router/index.html

The default TLS certificate is expected to be found in the default location: secret `router-certs` in default namespace.
This should be [populated by Puppet](https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/blob/qa/code/manifests/utils/certificates/openshift_services.pp).

In order to adjust some new features, we need a customized HAProxy router for Openshift.

We apply the instructions in the [Openshift documentation](https://docs.openshift.org/latest/install_config/router/customized_haproxy_router.html#router-configuration-template)
and in particular the use of a configmap `customrouter` to replace the default configuration template.
We also use the same configmap to set values specific to each environment: a password for haproxy stats
and the routers' [canonical hostname](https://docs.openshift.org/3.6/install_config/router/default_haproxy_router.html#finding-router-hostname)
 (the DNSLB alias)

Features:
* allow Intranet IPs only by default ([CIPAAS-165](https://its.cern.ch/jira/browse/CIPAAS-165)). To expose route to all IPs use route annotation `router.cern.ch/network-visibility: Internet`
* Block Technical Network IPs by default ([CIPAAS-231](https://its.cern.ch/jira/browse/CIPAAS-231)). To allow IPs from Technical Network use route annotation `router.cern.ch/technical-network-visibility: "true"`
* allow excluding certain routes from HAProxy routing ([CIPAAS-194](https://its.cern.ch/jira/browse/CIPAAS-194)). Use route label `router.cern.ch/exclude: true`
* DNSLB integration with pod lifecycle (details below)
* Special feature to support GitLab ([VCS-582](https://its.cern.ch/jira/browse/VCS-582)): bind a route to a dedicated port (for SSH service) with annotation `router.cern.ch/use-dedicated-port: 7999,:::7999` (for both IPv4 and IPv6),
only when environment variable `CERN_ALLOW_ROUTES_ON_DEDICATED_PORTS` is set to `true`

We also send logs to central monitoring via https://gitlab.cern.ch/paas-tools/paas-infra/haproxy-logging

## DNSLB integration

We use lifecycle hooks to HAProxy pods to integrate with DNSLB. This assumes that a node is considered valid for DNSLB
if the HAProxy pod is in the Running state and Ready, but not Terminating. See https://gitlab.cern.ch/ai/it-puppet-hostgroup-paas/merge_requests/43
for a script that can be used by lbclient to determine node suitability for DNSLB.

When a HAProxy pods needs to be terminated, to minimize impact we should wait for the following to happen:
* the node hosting the pod detects that the pod enters Terminating state, and will not consider itself valid for DNSLB anymore
* the LB master queries node status and removes its IP address from the DNSLB alias (checks every 5 minutes)
* the updated DNS record propagates to all client DNS cache/local servers (TTL is set to 60 seconds, but in practice at least 10 minutes)
* clients stop opening new connections via that node's IP address (following DNS updates)
* existing connections are closed (can take several hours in case of large downloads, websocket connections etc.)

A reasonable middle ground is to wait for 15 minutes, then wait up to 30 more minutes for connection count to reach 0.

To support this long termination process (15 to 45 minutes) we need to set:
* the HAProxy container's `terminationGracePeriodSeconds` to 50 minutes (this is how long Openshift will wait for the container to terminate gracefully)
* the `timeoutSeconds` of the `RollingDeploymentStrategyParams` to 1 hour (this is how long the deployment pod waits
for a change in number of replicas to actually happen; when scaling up new deployments it needs to wait for old pods to terminate gracefully
before the new ones can be started)

In case of testing a new router image in `test` enviroment we can **kill** the `haproxy-router` pod in orther to make it faster,
```bash
oc delete pods <pod> --grace-period=0 --force
```

NB: a deployment pod currently has an [hardcoded max lifetime](https://github.com/openshift/origin/blame/f2238ea34d95e7b56edb5b0678023e6ed4c0e8a8/pkg/deploy/api/types.go#L22)
of 6 hours. In case we run more HAProxy instances, if we wait up to 1h for a scaling operation we can't have more than 6 of them in total.
The `maxUnavailable` value of the `RollingDeploymentStrategyParams` becomes important: with the default value of 25%, we're fine: the deployment
process will not do more than 4 scaling operations in total (i.e. it will start stopping several HAProxy pods in parallel if there are more than
4 pods)

## Deploy to a new Openshift cluster

First, create the default router to initialize a `router` service account with appropriate permissions, a service and a deploymentconfig.
We will replace the deploymentconfig with our customized version, so we just use the default values for initialization.

```bash
oc adm router --service-account=router -n default
# Rename the default certificate used for securing the metrics endpoint (as by default has the same name as the certificate for the websites e.g. *.web.cern.ch).
# The name is important as this is what is later referenced in the template
oc annotate service/router -n default --overwrite service.alpha.openshift.io/serving-cert-secret-name=router-metrics-cert
# For DNSLB integration, we need a hostPath. This means we need to give higher privileges to the router serviceaccount
# (default is HostNetwork SCC only)
 oc adm policy add-scc-to-user privileged system:serviceaccount:default:router
```

Then apply the customized DeploymentConfiguration to:
* set environment variables for Intranet/Internet support and ignore routes with label `router.cern.ch/exclude=true`
* configure DNSLB integrate with a `preStop` lifecycle script and adjust timeouts for pod deployments (see dedicated section about DNSLB integration)
* use customized HAProxy config template

```bash
# generate a random password for stats. Note that this is not used anymore for Prometheus to collect metrics,
# serviceaccount token auth is used instead: cf. https://gitlab.cern.ch/paas-tools/prometheus-openshift
stats_pwd=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1)
oc create configmap customrouter -n default \
  --from-literal=STATS_PASSWORD="${stats_pwd}" \
  --from-file=conf/haproxy-config.template
# for DEV use deploy/parameters/dev
param_file=deploy/parameters/prod
oc process --local -f deploy/default-router-template.yaml --param-file="${param_file}" \
  | oc replace -n default -f -

# Set custom label in router Service selector.
# As we use a custom label to avoid automatic changes during OpenShift upgrades, change the selector of the service
# Changing this is harmless as the Service is not use for applications using the router
# Use `jq` as `oc patch` does not allow to delete the old selector.
oc get svc router -n default -o json | jq 'del(.spec.selector.router) | .spec.selector."openshift.cern.ch/router" = "router"' | oc replace -f -

# enable delegated auth for metrics. This is needed so router metrics can use openshift authentication
 oc adm policy add-cluster-role-to-user system:auth-delegator -z router -n default
# create a serviceaccount for automation of update deployment with GitLab CI and obtain its auth token
oc create serviceaccount gitlabci-deployer -n default
oc policy add-role-to-user edit -z gitlabci-deployer -n default
oc serviceaccounts get-token gitlabci-deployer -n default
```

To enable manually-triggered but automated deployment of changes from GitLab CI:
* set secret variables in the GitLab project `DEPLOY_TOKEN_DEV` and `DEPLOY_TOKEN_PROD`
from the token of `gitlabci-deployer` in dev and prod Openshift clusters respectively
* set [pipeline timeout](https://gitlab.cern.ch/paas-tools/custom-haproxy-router/settings/ci_cd) to 370 minutes
as rollout can be very long (up to 6h as per the DeploymentConfig max `activeDeadlineSeconds`)

## Updating to new version of the upstream HAProxy router

To deploy a new version:
* update the tag for the `ROUTER_IMAGE` parameter in [deploy/default-router-template.yaml](deploy/default-router-template.yaml)
* inject customizations from [our custom version of the template](conf/haproxy-config.template) into
  the [upstream template](https://github.com/openshift/origin/blob/master/images/router/haproxy/conf/haproxy-config.template)
  (making sure to select the tag/branch for the version of the router we want to deploy).
  Our customization are marked with `{{/*** CERN Customization */}}` comments.
  * What I found easiest for significant changes between versions is
    to take the new template, look for each `{{/*** CERN Customization */}}` part in our custom version
    and insert them in the corresponding place in the new template.
  * But sometimes there are only very minor changes and it's easier to replicate them in our custom template.
    E.g. for small differences between 3.7 and 3.9, clone https://github.com/openshift/origin
    and check `git diff v3.7.1..v3.9.0 -- images/router/haproxy/conf/haproxy-config.template` - there's a single line of difference.
* test it with Jenkins (see section below)
* to deploy, execute the [manual deployment jobs in GitLab CI](https://gitlab.cern.ch/paas-tools/custom-haproxy-router/pipelines)

GitLab router must also be updated with the new custom configuration template.
Follow the [procedure to update GitLab routers](https://gitlab.cern.ch/vcs/gitlab-docs/blob/master/operations/updaterouters.md).

## Testing with Jenkins

* Create web app `test-custom-haproxy-router.web.cern.ch` from WebServices
* Set quota: medium (we need many pods)
* [Create a jenkins-cern instance](https://cern.ch/jenkinsdocs), admin e-group: openshift-admins
* As project admin, set permissions for serviceaccounts:

```bash
# allow the slave pods read access to the API, so they can look up HAProxy pod IP addresses (alternatively we could use services with type DNS and a DNS lookup)
oc adm policy add-role-to-user -n test-custom-haproxy-router -z default view
# the router instances needs to read endpoints and routes. It will complain about not being able to read all services/endpoints but it's OK for our tests.
oc adm policy add-role-to-user -n test-custom-haproxy-router -z router view
# the router also needs to be able to update route status
oc adm policy add-role-to-user -n test-custom-haproxy-router -z router system:router
```

* As Openshift cluster admin:

```bash
#  Whitelist namespace for technical network routes so we can actually test them
oc label namespace test-custom-haproxy-router 'router.cern.ch/technical-network-allowed=Internet'
```

* Configure a job in Jenkins to build using the [Jenkinsfile](test/Jenkinsfile) in this project (see the provided [Jenkins job XML definition](test/job.xml))
* To test, trigger the build manually from https://test-custom-haproxy-router.web.cern.ch (providing the name of the branch to build)

NB: to avoid the complications of [integration with GitLab](https://jenkinsdocs.web.cern.ch/chapters/revision-control-systems/git/gitlab/gitlab-plugin.html)
we simply set up a job to be triggered manually.


